﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Grpc.Core;
using Woodvillage.Example;

namespace communication.server
{
    public class PlaygroundImpl : ExampleService.ExampleServiceBase
    {
        public override async Task BidiStream(IAsyncStreamReader<GetMessageRequest> requestStream, IServerStreamWriter<GetMessageResponse> responseStream, ServerCallContext context)
        {
            Console.WriteLine("-----START-----");

            while (await requestStream.MoveNext(context.CancellationToken))
            {
                Console.WriteLine(requestStream.Current.Id);
                await Task.Delay(TimeSpan.FromSeconds(2));
                await responseStream.WriteAsync(new GetMessageResponse
                {
                    Message = Guid.NewGuid().ToString()
                });
            }

            Console.WriteLine("------END------");
        }
    }
}
