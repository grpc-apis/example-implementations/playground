﻿using Grpc.Core;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading.Tasks;
using Woodvillage.Example;

namespace communication.server
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = new HostBuilder()
              .ConfigureServices((hostContext, services) =>
              {
                  Server server = new Server
                  {
                      Services =
                      {
                          ExampleService.BindService(new PlaygroundImpl())
                      },
                      Ports = { new ServerPort("127.0.0.1", 50200, ServerCredentials.Insecure) }
                  };
                  server.Start();
              });

            await builder.RunConsoleAsync();
        }
    }
}
