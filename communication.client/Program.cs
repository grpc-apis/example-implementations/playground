﻿using Grpc.Core;
using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Woodvillage.Example;

namespace communication.client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));

            Channel channel = new Channel("127.0.0.1", 50200, ChannelCredentials.Insecure);
            ExampleService.ExampleServiceClient client = new ExampleService.ExampleServiceClient(channel);

            var c = Observable.Create<string>(async observer =>
            {
                using (var call = client.BidiStream())
                {
                    var requestStream = call.RequestStream;
                    var responseStream = call.ResponseStream;

                    while (!channel.ShutdownToken.IsCancellationRequested)
                    {
                        await requestStream.WriteAsync(new GetMessageRequest
                        {
                            Id = Guid.NewGuid().ToString()
                        });
                        try
                        {
                            await responseStream.MoveNext(channel.ShutdownToken);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        observer.OnNext(responseStream.Current.Message);
                    }
                }
            });
            c.Subscribe(message => Console.WriteLine(message));

            Console.WriteLine("------ Running ------");
            Console.ReadKey();

            Console.WriteLine("------ Stopping -----");
            await channel.ShutdownAsync();

            Console.ReadKey();
        }
    }
}
